DIR=/nethome/wmartin45/.linuxbrew
CC = $(DIR)/bin/g++-5
CFLAGS = -O3 -I$(DIR)/include
OMPFLAGS = -fopenmp -std=c++11 -L$(DIR)/lib -lrt
RM = rm -f
NINTERVALS = 20
INFILE = data.xyz
OUTFILE = out.xyz
NTHREADS = 32
#JOBID = 1

all: runharness

%.o: %.cpp
	$(CC) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

proj1.o: forces.h
forces.o: forces.h

gendata: gendata.o
	$(CC) -o $@ $^

proj1: main.o box.o particle.o 
	$(CC) $(CFLAGS) $(OMPFLAGS) -o $@ $^

clean:
	$(RM) *.o proj1 gendata

runharness: proj1
	./proj1 -i $(NINTERVALS) -f $(INFILE) -o $(OUTFILE) -t $(NTHREADS)
#	./proj1 -i $(NINTERVALS) -f $(INFILE) -o $(OUTFILE).$(JOBID) -t $(NTHREADS)
