#if !defined (BOX_H)
#define BOX_H

#include "cell.h"
#include <vector>
#include <utility>
#include <map>
#include "random.h"

typedef std::pair<Particle*, Particle*> ParticlePair;
typedef std::vector< ParticlePair > PairArray;

class Box
{
public:
    typedef std::vector<Particle*> ParticleVector;

    Box(Point dimensions, float particle_radius) ;

    void init(int size, int seed);

    inline void add_particle(Particle& p)
    {
        p.position.x = 
            remainder(p.position.x - _dimensions.x/2, _dimensions.x) + _dimensions.x/2;
        p.position.y = 
            remainder(p.position.y - _dimensions.y/2, _dimensions.y) + _dimensions.y/2;
        p.position.z = 
            remainder(p.position.z - _dimensions.z/2, _dimensions.z) + _dimensions.z/2;

        int x = p.position.x/_celldim.x;
        int y = p.position.y/_celldim.y;
        int z = p.position.z/_celldim.z;

        p.id = _particles.size();
        _particles.push_back(p);
        (*this)(x,y,z).add_particle(& _particles[p.id]);
    }

    inline bool contains(const Particle& p)
    {
        return 
            (p.position.x >= 0 && p.position.x < _dimensions.x) &&
            (p.position.y >= 0 && p.position.y < _dimensions.y) && 
            (p.position.z >= 0 && p.position.z < _dimensions.z);
    }

    inline int x(int i)
    {
        return i % _cols;
    }
    inline int y(int i)
    {
        return (i / _cols) % _rows;
    }

    inline int z(int i)
    {
        return i / (_rows * _cols);
    }

    inline int get_index(int col, int row, int level)
    {
        return (level * _cols * _rows) + (row * _cols) + col;
    }

    inline Cell& operator ()(int col, int row, int level)
    {
        return _cells[get_index(col, row, level)];
    }

    void simulate(int time);
    void step();
    inline std::vector<Particle>& get_particles() {
      return _particles;
    }

    void gather_pairs(PairArray& pairs, int &size, std::vector<int>& cells);

private:
    std::vector<Cell> _cells;
    std::vector<Particle> _particles;
    Point _dimensions;
    Point _celldim;
    int _cols; // = x 
    int _rows; // = y 
    int _levels; // = z 

    // bookkeeping members--these are here to make sure that memory allocations only occur once
    PairArray pairs;
    std::vector< std::vector<int> > worklist;
    std::map< int, std::vector<Particle*> > _vectors;

    static __thread Random *_rng;
};


#endif 
