#if !defined(CELL_H)
#define CELL_H

#include <vector>
#include "particle.h"
#include <omp.h>

class Cell
{
private:
    // map may not offer best performance but try it first and see: map suffers from
    // reorder operations and poor cacheability--especially when iterating over all elements
    // problem with list and vector is that requires linear search time for removal
    // additions are always constant time
    // vectors have to perform an copy operation of all remaining elements when item is removed
    // list may be better if removes are done "inline" as we traverse particles to 
    // determine if they have moved a cell.
    //
    omp_lock_t _lock;
    std::vector<Particle*> _particles;
    Point _origin;
    Point _dimensions;

public:
    inline Cell(Point origin, Point dimensions) : _origin(origin), _dimensions(dimensions)
    {
        omp_init_lock(&_lock);
    }

    inline ~Cell() { omp_destroy_lock(&_lock); }

    std::vector<Particle*>::iterator begin() { return _particles.begin(); }
    std::vector<Particle*>::iterator end() { return _particles.end(); }

    int size() { return _particles.size(); }

    inline bool contains(const Particle& p)
    {
        return 
            (p.position.x >= _origin.x && p.position.x < _origin.x + _dimensions.x) &&
            (p.position.y >= _origin.y && p.position.y < _origin.y + _dimensions.y) && 
            (p.position.z >= _origin.z && p.position.z < _origin.z + _dimensions.z);
    }

    inline void add_particle(Particle* p)
    {
	omp_set_lock(&_lock);
        _particles.push_back(p);
	omp_unset_lock(&_lock);
    }

    inline void clear()
    {
        _particles.clear();
    }
};


#endif
