
- The nonlinear runtime in the number of intervals is strange.  I understand
  you have some setup of your precomputed cell pairs, but that doesn't explain
  why I time step late in the simulation should take less time than a time
  step early in the simulation.  I suspect this may be because, due to the
  fact that you are not computing force pairs across the periodic boundary,
  the particles are spreading out toward the boundaries, resulting in fewer
  interactions.
