#if !defined (PARTICLE_H)
#define PARTICLE_H

#include <cmath>
#include <stdint.h>

struct Point
{
    double x, y, z;

    inline Point(double i, double j, double k) : x(i), y(j), z(k) {}
    inline double distance(Point p)
    {
        return sqrt( (p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) + (p.z - z)*(p.z - z) );
    }
};

class Particle
{
public:
    int id;
    // int type;
    Point position;  // the position of the particle

    inline Particle() : position(0,0,0), _force(0,0,0), id(0)
    {
    }

    #pragma omp declare simd notinbranch
    inline void apply_force(const double& x, const double& y, const double& z, double force)
    {
        #pragma omp atomic
        _force.x += x * force;
        #pragma omp atomic
        _force.y += y * force;
        #pragma omp atomic
        _force.z += z * force;
    }

    #pragma omp declare simd inbranch
    inline void apply_force_branched(const double& x, const double& y, const double& z, double force)
    {
        #pragma omp atomic
        _force.x += x * force;
        #pragma omp atomic
        _force.y += y * force;
        #pragma omp atomic
        _force.z += z * force;
    }


    inline void displace()
    {
        position.x += _force.x;
        position.y += _force.y;
        position.z += _force.z;
        _force.x = _force.y = _force.z = 0;
    }

private:
    Point _force;     // the force being applied during this time step
};


#endif
